package com.my.dto;
import java.io.Serializable;

/**
 * This class represents Data Transfer Object(DTO) for REST service response.
 *
 * @author qxk5116
 *
 * @param <T>
 *            Response type
 */
public class ResponseDTO<T> implements Serializable {

	/**
	 * default version
	 */
	private static final long serialVersionUID = 1L;

	private Status status;
	private T items;

	/**
	 * Prepares default instance with success (No Error) status
	 */
	public ResponseDTO() {
		this.status = new Status("No Error");
	}

	/**
	 * Prepares DTO instance of status mentioned and items as response
	 *
	 * @param status
	 *            status of response
	 * @param items
	 *            data of requested resource
	 */
	public ResponseDTO(final Status status, final T items) {
		super();
		this.status = status;
		this.items = items;
	}

	/**
	 * Prepares DTO instance of status mentioned and items as response
	 *
	 * @param status
	 *            status of response
	 * @param code
	 *            error code
	 * @param items
	 *            data of requested resource
	 */
	public ResponseDTO(final String value, final Integer code, final T items) {
		super();
		this.status = new Status(value, code);
		this.items = items;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return this.status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(final Status status) {
		this.status = status;
	}

	/**
	 * @return the items
	 */
	public T getItems() {
		return this.items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(final T items) {
		this.items = items;
	}
}
