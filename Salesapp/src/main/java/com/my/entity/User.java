package com.my.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	public static final String USERNAME = "username";
	
	public static final String PASSWORD = "password";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false,name="id")
	private int id;

	@Column(name="password",length=500)
	private String password;

	@Column(name="user_name",length=500)
	private String username;
	
	@Column(name="first_name",length=255)
	private String firstName;
	
	@Column(name="last_name",length=255)
	private String lastName;
	
	@OneToOne
	@JoinColumn(name="role_id")
	private RoleMaster userRole;
	
	@OneToOne
	@JoinColumn(name="branch_id")
	private Branch branch;

	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public RoleMaster getUserRole() {
		return userRole;
	}

	public void setUserRole(RoleMaster userRole) {
		this.userRole = userRole;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

}