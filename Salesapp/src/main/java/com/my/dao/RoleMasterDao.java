package com.my.dao;

import org.springframework.stereotype.Component;

import com.my.entity.RoleMaster;
import com.my.entity.User;

@Component
public interface RoleMasterDao extends GenericDao<RoleMaster>{

}
