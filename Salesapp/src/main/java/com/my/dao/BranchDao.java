package com.my.dao;

import org.springframework.stereotype.Component;

import com.my.entity.Branch;
import com.my.entity.RoleMaster;
import com.my.entity.User;

@Component
public interface BranchDao extends GenericDao<Branch>{

}
