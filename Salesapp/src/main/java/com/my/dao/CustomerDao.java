package com.my.dao;

import org.springframework.stereotype.Component;

import com.my.entity.Customer;

@Component
public interface CustomerDao extends GenericDao<Customer>{

}
