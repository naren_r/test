package com.my.dao;

import org.springframework.stereotype.Component;

import com.my.entity.Product;

@Component
public interface ItemDao extends GenericDao<Product>{

}
