package com.my.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.dto.AddProductDTO;
import com.my.dto.AddProductQuantityDTO;
import com.my.dto.Status;
import com.my.entity.Product;
import com.my.service.ProductService;
import com.my.utility.RESTServiceException;

@Controller
@RequestMapping("/items")
public class ProductController {

	final static Logger logger = Logger.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Status addProduct(@RequestBody AddProductDTO product) {
		if (logger.isDebugEnabled()) {
			logger.debug("addProduct (" + product + ") called!");
		}
		try {
			productService.addPorduct(product);
			Status status = new Status("Success", 200);
			return status;
		} catch (RESTServiceException e) {
			logger.error(e);
			return e.getStatus();
		}
	}

	@RequestMapping(value = "/addquantity", method = RequestMethod.POST)
	@ResponseBody
	public Status addProductQuantity(@RequestBody AddProductQuantityDTO dto) {
		if (logger.isDebugEnabled()) {
			logger.debug("addProductQuantity (" + dto + ") called!");
		}
		try {
			productService.addProductQuatity(dto);
			Status status = new Status("Success", 200);
			return status;
		} catch (RESTServiceException e) {
			logger.error(e);
			return e.getStatus();
		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public List<Product> findAllProduct() {
		if (logger.isDebugEnabled()) {
			logger.debug("findAllProduct () called!");
		}
		try {
			return productService.getAllProduct();
		} catch (RESTServiceException e) {
			logger.error(e);
		}
		return null;
	}
}
