package com.my.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.dto.Status;
import com.my.dto.UserDTO;
import com.my.entity.User;
import com.my.service.UserService;
import com.my.utility.RESTServiceException;

@Controller
public class LoginController {

	final static Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String test() {
		if (logger.isDebugEnabled()) {
			logger.debug("Inside test()");
		}
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public UserDTO validateUser(@RequestBody User user) {
		if (logger.isDebugEnabled()) {
			logger.debug("Inside validateUser(" + user + ") called!");
		}
		UserDTO response = new UserDTO();
		try {
			User loginedUser = userService.validateUser(user);
			Status status = new Status("SUCCESS", 200);
			response.setStatus(status);
			response.setRole(loginedUser.getUserRole().getUserRole());
			response.setUsername(loginedUser.getUsername());
			response.setFirstname(loginedUser.getFirstName());
			response.setLastname(loginedUser.getLastName());
			return response;
		} catch (RESTServiceException e) {
			logger.error(e);
			response.setStatus(e.getStatus());
			return response;
		}
	}
}
