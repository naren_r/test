package com.my.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.dto.AddUserDTO;
import com.my.dto.RolesDTO;
import com.my.dto.Status;
import com.my.entity.Product;
import com.my.service.UserService;
import com.my.utility.RESTServiceException;

@Controller
@RequestMapping("/user")
public class UserController {

	final static Logger logger = Logger.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/roles",method=RequestMethod.GET)
	@ResponseBody
	public RolesDTO getUserRoles(){
		if(logger.isDebugEnabled()){
			logger.debug("getUserRoles() called!");
		}
		RolesDTO response = new RolesDTO();
		try{
			List<String> listRoles = userService.getRoles();
			response.setRoles(listRoles);
			response.setStatus(new Status("SUCCESS",200));
		}catch(RESTServiceException ex){
			logger.error(ex);
			response.setStatus(ex.getStatus());
		}
		return response;
	}
	
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public Status saveUser(@RequestBody AddUserDTO user){
		if(logger.isDebugEnabled()){
			logger.debug("saveUser(" + user + ") called!");
		}
		try{
			userService.addUser(user);
			Status status = new Status("SUCCESS", 200);
			return status;	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return exp.getStatus();
		}
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public Status deleteUser(@RequestBody AddUserDTO user){
		if(logger.isDebugEnabled()){
			logger.debug("deleteUser(" + user + ") called!");
		}
		try{
			userService.deleteUser(user);
			Status status = new Status("SUCCESS", 200);
			return status;	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return exp.getStatus();
		}
	}
}
