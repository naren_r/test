package com.my.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.dto.AddBranchDTO;
import com.my.dto.AddCustomerDTO;
import com.my.dto.ResponseDTO;
import com.my.dto.Status;
import com.my.entity.Branch;
import com.my.service.BranchService;
import com.my.service.CustomerService;
import com.my.service.UserService;
import com.my.utility.RESTServiceException;

@Controller
@RequestMapping("/branch")
public class BranchController {

	final static Logger logger = Logger.getLogger(BranchController.class);

	@Autowired
	CustomerService customerService;

	@Autowired
	UserService userService;

	@Autowired
	BranchService branchService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Status addBranch(@RequestBody AddBranchDTO dto) {
		if (logger.isDebugEnabled()) {
			logger.debug("addBranch(" + dto + ") called!");
		}
		try {
			this.branchService.addBranch(dto);
			Status status = new Status("SUCCESS", 200);
			return status;
		} catch (RESTServiceException exp) {
			logger.error(exp);
			return exp.getStatus();
		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseDTO<List<Branch>> getAllBranch() {
		if (logger.isDebugEnabled()) {
			logger.debug("getAllBranch called!");
		}
		ResponseDTO<List<Branch>> response = new ResponseDTO<List<Branch>>();
		try {
			response.setItems(this.branchService.getAllBranch());
			response.setStatus(new Status("SUCCESS", 200));
		} catch (RESTServiceException exp) {
			logger.error(exp);
			response.setStatus(exp.getStatus());
		}
		return response;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public Status editBranch(@RequestBody AddBranchDTO dto) {
		if (logger.isDebugEnabled()) {
			logger.debug("editBranch(" + dto + ") called!");
		}
		try {
			this.branchService.editBranch(dto);
			Status status = new Status("SUCCESS", 200);
			return status;
		} catch (RESTServiceException exp) {
			logger.error(exp);
			return exp.getStatus();
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Status deleteBranch(@RequestBody AddBranchDTO dto) {
		if (logger.isDebugEnabled()) {
			logger.debug("deleteBranch called!");
		}
		try {
			this.branchService.deleteBranch(dto);
			Status status = new Status("SUCCESS", 200);
			return status;
		} catch (RESTServiceException exp) {
			logger.error(exp);
			return exp.getStatus();
		}
	}
}
