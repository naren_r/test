package com.my.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.my.dto.AddCustomerDTO;
import com.my.dto.AddUserDTO;
import com.my.dto.Status;
import com.my.entity.Customer;
import com.my.service.CustomerService;
import com.my.service.UserService;
import com.my.utility.RESTServiceException;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	final static Logger logger = Logger.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	public Status addCustomer(@RequestBody AddCustomerDTO dto){
		if(logger.isDebugEnabled()){
			logger.debug("saveUser(" + dto + ") called!");
		}
		try{
			this.customerService.addCustomer(dto);
			Status status = new Status("SUCCESS", 200);
			return status;	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return exp.getStatus();
		}
	}
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	@ResponseBody
	public List<Customer> getAllCustomer(){
		if(logger.isDebugEnabled()){
			logger.debug("getAllCustomer called!");
		}
		try{
			return this.customerService.getAllCustomer();	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return null;
		}
	}
	
	@RequestMapping(value="/edit",method=RequestMethod.POST)
	@ResponseBody
	public Status editCustomer(@RequestBody AddCustomerDTO dto){
		if(logger.isDebugEnabled()){
			logger.debug("editCustomer(" + dto + ") called!");
		}
		try{
			this.customerService.editCustomer(dto);
			Status status = new Status("SUCCESS", 200);
			return status;	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return exp.getStatus();
		}
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	@ResponseBody
	public Status deleteCustomer(@RequestBody AddCustomerDTO dto){
		if(logger.isDebugEnabled()){
			logger.debug("deleteUser called!");
		}
		try{
			this.customerService.deleteCustomer(dto);
			Status status = new Status("SUCCESS", 200);
			return status;	
		}catch(RESTServiceException exp){
			logger.error(exp);
			return exp.getStatus();
		}
	}
}
