package com.my.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.controller.LoginController;
import com.my.dao.ItemDao;
import com.my.dao.UserDao;
import com.my.dto.AddProductDTO;
import com.my.dto.AddProductQuantityDTO;
import com.my.entity.Product;
import com.my.entity.User;
import com.my.utility.RESTServiceException;

@Service
public class ProductService {
	
	final static Logger logger = Logger.getLogger(ProductService.class);

	@Autowired
	private ItemDao itemDao;
		
	@Autowired
	private UserDao userDao;
		

	@Transactional(readOnly=false)
	public void addPorduct(AddProductDTO product) throws RESTServiceException {
		if(logger.isDebugEnabled()){
			logger.debug("addPorduct in called!");
		}
		Product newProduct = new Product();
		newProduct.setName(product.getName());
		newProduct.setDescription(product.getDescription());
		newProduct.setPrice(product.getPrice());
		newProduct.setUnit(product.getUnit());
		User user= this.userDao.findOneByProperty(User.USERNAME,product.getUsername());
		if(user == null){
			throw new RESTServiceException("Invalid User!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		newProduct.setUser(user);
		this.itemDao.create(newProduct);
	}
	
	@Transactional(readOnly=false)
	public void addProductQuatity(AddProductQuantityDTO dto) throws RESTServiceException {
		if(logger.isDebugEnabled()){
			logger.debug("addProductQuatity() in called!");
		}
		Product product =  this.itemDao.findOneByProperty(Product.NAME,dto.getName());
		if(product == null){
			throw new RESTServiceException("Invalid productname!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		product.setQuntity(dto.getQuantity());
		this.itemDao.edit(product);
	}
	
	@Transactional(readOnly=false)
	public List<Product> getAllProduct() throws RESTServiceException {
		if(logger.isDebugEnabled()){
			logger.debug("getAllProduct() in called!");
		}
		List<Product> listProduct = this.itemDao.findAll();
		return listProduct;
	}
}
