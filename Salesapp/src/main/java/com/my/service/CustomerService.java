package com.my.service;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.my.dao.CustomerDao;
import com.my.dao.UserDao;
import com.my.dto.AddCustomerDTO;
import com.my.dto.AddUserDTO;
import com.my.entity.Customer;
import com.my.entity.User;
import com.my.utility.RESTServiceException;

@Component
public class CustomerService {

	final static Logger logger = Logger.getLogger(CustomerService.class);
	
	@Autowired
	private CustomerDao customerDao;


	@Autowired
	private UserDao userDao;

	
	@Transactional
	public void addCustomer(AddCustomerDTO dto) throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("addCustomer() called!");	
		}
		User user = userDao.findOneByProperty(User.USERNAME,dto.getUsername());
		if(user == null){
			throw new RESTServiceException("User does not exist!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		Customer customer = new Customer();
		customer.setShopName(dto.getShopname());
		customer.setFirstName(dto.getFirstname());
		customer.setLastName(dto.getLastname());
		customer.setAddress(dto.getAddres());
		customer.setCity(dto.getCity());
		customer.setPhoneNumber(dto.getPhonenumber());
		customer.setUser(user);
		this.customerDao.create(customer);
	}
	
	@Transactional
	public List<Customer> getAllCustomer() throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("getAllCustomer() called!");	
		}
		return this.customerDao.findAll();
	}
	
	@Transactional
	public void editCustomer(AddCustomerDTO dto) throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("editCustomer called!");	
		}
		Customer customer = this.customerDao.findOneByProperty(Customer.SHOPNAME,dto.getShopname());
		if(customer == null){
			throw new RESTServiceException("Invalid Customer!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		customer.setFirstName(dto.getFirstname());
		customer.setLastName(dto.getLastname());
		customer.setAddress(dto.getAddres());
		customer.setCity(dto.getCity());
		customer.setPhoneNumber(dto.getPhonenumber());
		this.customerDao.edit(customer);
	}
	
	@Transactional
	public void deleteCustomer(AddCustomerDTO dto) throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("deleteCustomer called!");	
		}
		Customer customer = this.customerDao.findOneByProperty(Customer.SHOPNAME,dto.getShopname());
		if(customer == null){
			throw new RESTServiceException("Invalid Customer!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		this.customerDao.remove(customer);
	}
}
