package com.my.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.my.dao.BranchDao;
import com.my.dao.RoleMasterDao;
import com.my.dao.UserDao;
import com.my.dto.AddUserDTO;
import com.my.entity.Branch;
import com.my.entity.RoleMaster;
import com.my.entity.User;
import com.my.utility.RESTServiceException;

@Component
public class UserService {

	final static Logger logger = Logger.getLogger(UserService.class);
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleMasterDao roleMasterDao;
	
	@Autowired
	private BranchDao branchDao;

	
	@Transactional
	public User validateUser(User user) throws RESTServiceException{
		logger.info("validateUser (" + user + ") called!");
		User logedInUser = userDao.findOneByProperty(User.USERNAME,user.getUsername(),User.PASSWORD,user.getPassword());
		if(logedInUser != null){
			return logedInUser;
		}else{
			throw new RESTServiceException("Invalid Username or Password!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
	}
	
	@Transactional
	public List<String> getRoles() throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("getRoles() called!");
			
		}
		List<String> listRoles = new ArrayList<String>();
		List<RoleMaster> listRole = roleMasterDao.findAll();
		for(RoleMaster rm : listRole){
			listRoles.add(rm.getUserRole());
		}
		return listRoles;
	}
	
	@Transactional
	public void addUser(AddUserDTO userDTO) throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("addUser() called!");	
		}
		RoleMaster role = this.roleMasterDao.findOneByProperty(RoleMaster.USERROLE,userDTO.getRole());
		Branch branch = this.branchDao.findOneByProperty(Branch.NAME,userDTO.getBranchname());
		User user1 = userDao.findOneByProperty(User.USERNAME,userDTO.getUsername());
		if(user1 != null){
			throw new RESTServiceException("Username Already Exist,Please select different username!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		if(role == null){
			throw new RESTServiceException("Invalid User Role!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		if(branch == null){
			throw new RESTServiceException("Invalid Branch Name!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		User newUser = new User();
		newUser.setFirstName(userDTO.getFirstname());
		newUser.setLastName(userDTO.getLastname());
		newUser.setUsername(userDTO.getUsername());
		newUser.setPassword(userDTO.getPassword());
		newUser.setUserRole(role);
		newUser.setBranch(branch);
		userDao.create(newUser);
	}
	
	@Transactional
	public void editUser(AddUserDTO userDTO) throws RESTServiceException{
	}
	
	@Transactional
	public void deleteUser(AddUserDTO userDTO) throws RESTServiceException{
		if(logger.isDebugEnabled()){
			logger.debug("deleteUser(userName = " + userDTO.getUsername() + ") called!");	
		}
		User user = userDao.findOneByProperty(User.USERNAME,userDTO.getUsername());
		if(user == null){
			throw new RESTServiceException("User not exist!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					null);
		}
		userDao.remove(user);
	}
}
